----------

Installation
--------------
launch `make build_docker` command.

RabbitMQ
--------
A rabbitmq image with bindings/queues/exchanges for our microservices configurated.

#### <i class="icon-key"></i> Connexion
http://localhost:15672/

|Login|Password|
|-----|--------|
|root |root    |
