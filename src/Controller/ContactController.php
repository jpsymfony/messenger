<?php

declare(strict_types=1);

namespace App\Controller;

use App\DTO\ContactDTO;
use App\Form\Type\ContactType;
use App\Messenger\Message\ContactEmailMessage;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class ContactController
{
    private $formFactory;
    private $messageBus;
    private $twig;
    private $router;

    public function __construct(
        Environment $twig,
        FormFactoryInterface $formFactory,
        UrlGeneratorInterface $router,
        MessageBusInterface $messageBus
    ) {
        $this->formFactory = $formFactory;
        $this->messageBus = $messageBus;
        $this->twig = $twig;
        $this->router = $router;
    }

    /**
     * @Route(
     *     name="contact",
     *     path="/",
     *     methods={"GET", "POST"}
     * )
     */
    public function contact(Request $request, Session $session): Response
    {
        $dto = new ContactDTO();
        $form = $this->formFactory->create(ContactType::class, $dto)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // ContactEmailMessageNormalizer is mandatory because property fields are empty otherwise in the ContactEmailHandler
            $this->messageBus->dispatch(ContactEmailMessage::fromContactForm($dto));

            // ContactEmailMessageNormalizer works but is useful as property fields are filled in the ContactEmailHandler
//            $this->messageBus->dispatch(new ContactEmailMessage($dto->email, $dto->content, $dto->subject));

            $session->getFlashBag()->add('success', 'The email has been handled');

            return new RedirectResponse($this->router->generate('contact'));
        }

        return new Response($this->twig->render('contact/contact.html.twig', [
            'form' => $form->createView(),
        ]));
    }
}
