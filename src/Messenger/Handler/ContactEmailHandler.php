<?php

declare(strict_types=1);

namespace App\Messenger\Handler;

use App\Messenger\Message\ContactEmailMessage;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Twig\Environment;

final class ContactEmailHandler implements MessageHandlerInterface
{
    private $mailer;
    private $twig;

    public function __construct(
        \Swift_Mailer $mailer,
        Environment $twig
    ) {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function __invoke(ContactEmailMessage $message): void
    {
        $mail = (new \Swift_Message())
            ->setFrom($message->getEmail())
            ->setTo($message->getEmail())
            ->setBody($this->twig->render('emails/contact.html.twig', [
                'contact_message' => $message,
            ]), 'text/html')
            ->setSubject('New message !')
        ;

        $this->mailer->send($mail);
    }
}
