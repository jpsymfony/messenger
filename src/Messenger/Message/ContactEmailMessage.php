<?php

declare(strict_types=1);

namespace App\Messenger\Message;

use App\DTO\ContactDTO;

/**
 * The constructor is private to make sure we don't create any message from outside.
 * We have only static constructor: it directly exposes the different ways the
 * application creates messages.
 * In our case, two features only: form and serializer denormalization.
 */
class ContactEmailMessage
{
    private $email;
    private $subject;
    private $content;

    private function __construct()
    {
    }

    //if we want to try to pass the object directly from the controller
//    public function __construct(string $email, string $subject, string $content)
//    {
//        $this->email = $email;
//        $this->subject = $subject;
//        $this->content = $content;
//    }

    public static function fromContactForm(ContactDTO $dto): self
    {
        $message = new self();

        // if we want to try to pass object directly
//        $message = new self($dto->email, $dto->content, $dto->subject);

        $message->email = $dto->email;
        $message->subject = $dto->subject;
        $message->content = $dto->content;

        return $message;
    }

    public static function fromDenormalization(string $email, string $subject, string $content): self
    {
        $message = new self();

        // if we want to try to pass object directly
//        $message = new self($email, $subject, $content);

        $message->email = $email;
        $message->subject = $subject;
        $message->content = $content;

        return $message;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
