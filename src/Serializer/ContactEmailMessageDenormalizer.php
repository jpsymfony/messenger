<?php

declare(strict_types=1);

namespace App\Serializer;

use App\Messenger\Message\ContactEmailMessage;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class ContactEmailMessageDenormalizer implements DenormalizerInterface
{
    /**
     * {@inheritdoc}
     *
     * @return ContactEmailMessage
     */
    public function denormalize($data, $class, $format = null, array $context = [])
    {
        if (!isset($data['email'], $data['subject'], $data['content'])) {
            throw new UnexpectedValueException(\sprintf(
                'To denormalize a %s object, these fields are mandatory in the normalized data: %s',
                ContactEmailMessage::class, 'email,subject,content'
            ));
        }

        return ContactEmailMessage::fromDenormalization($data['email'], $data['subject'], $data['content']);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return ContactEmailMessage::class === $type;
    }
}
