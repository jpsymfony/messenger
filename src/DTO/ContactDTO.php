<?php

declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

final class ContactDTO
{
    /**
     * @Assert\NotBlank
     * @Assert\Email
     */
    public $email;

    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     */
    public $subject;

    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     */
    public $content;
}
