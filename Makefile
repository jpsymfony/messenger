build_docker: build up install

build:
	docker-compose build --build-arg USER=${USER} --build-arg UID=${UID} php

up:
	docker-compose up -d

ssh:
	docker exec -it php_messenger bash

install: composer

composer:
	docker exec -i php_messenger bash -c 'composer install --no-interaction -o'

setfacl:
	setfacl -dR -m u:"www-data":rwX -m u:$(whoami):rwX var
	setfacl -R -m u:"www-data":rwX -m u:$(whoami):rwX var

stop:
	-docker-compose stop

cache:
	docker exec -i php_messenger bash -c 'bin/console cache:clear --no-warmup && bin/console cache:warmup'

cache_all:
	docker exec -i php_messenger bash -c 'bin/console cache:clear --no-warmup --env dev && bin/console cache:warmup --env dev && bin/console cache:clear --no-warmup --env=test && bin/console cache:warmup --env=test && bin/console cache:clear --no-warmup --env prod && bin/console cache:warmup --env prod'

clean_docker:
	docker image prune --force && docker container prune --force

test_phpunit:
	docker exec -it php_factory_method bash -c 'vendor/bin/phpunit'

tests: test_phpunit test_behat

.PHONY: build up ssh install composer setfacl database stop cache cache_all clean_docker behat phpunit tests
